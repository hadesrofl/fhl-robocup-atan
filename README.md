Modified Version 0.44
=====================================

This is a branch of the Atan Repository on Github (https://github.com/robocup-atan/atan).

It is used for an university project to develop a AI for the RoboCup SoccerServer.

The Following changes happened while working on Atan for this project:

* Move(x,y) allows double values (former: integer)
* Fix Coach Hear (Say doesn't work though)
* Removed the pause in SServerPlayer
* Fix TurnNeckCommand in SServerPlayer (seems to not be called)
* Dash (power, dir) is capable of receiving a direction as parameter
* Capacity and Effort of ServerParams for Player Stats fixed
* Referee Message after catching a ball as goalie fixed
* Unknown Objects will be set to Player via infoUnknownObject. Unknown Objects might be a player, goal or flag
* PlayMode indirect free kick for right and left side implemented
* Referee Message for backpass implemented
* set cycle on: infoSenseBody. The cycle is the current turn of the server and game. Can be implemented for infoSee and infoHear as well (code is commented. Comments need to be deleted to get it to work)

Original Atan Readme
====================

Atan [![Build Status](https://travis-ci.org/robocup-atan/atan.png?branch=master)](https://travis-ci.org/robocup-atan/atan)
===============
Atan is an interface to the 2D Soccer Server of Robocup's simulation league. It should allow you to concentrate on the job of controlling your clients without worrying about all the behind the scenes stuff between SServer and the clients.

More details can be found at http://robocup-atan.github.com/atan

Requirements
------------
To build Atan for yourself, you'll need:
* Git
* Maven
* JDK 1.6

Usage
-----
Atan is designed to be included in your project as an external library. In order to build Atan, you need to run the following commands.
Maven will download any dependencies (such as [JavaCC](http://javacc.java.net)) and compile Atan for you.

<pre>
git clone git://github.com/robocup-atan/atan.git
cd atan
mvn clean install
</pre>

The generated .jar file can be found in your Maven repository, usually $HOME/.m2/repository/...

Example
-------
An example project using Atan can be found at https://github.com/robocup-atan/example-team

License
-------
Atan is free software released under the MIT License.
